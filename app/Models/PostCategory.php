<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PostCategory extends Model
{
    
    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'category_id', 'post_id'
    ];
    
    public function post()
    {
        return $this->hasOne('App\Models\Post', 'id', 'post_id');
    }
    
    public function category()
    {
        return $this->hasOne('App\Models\Category', 'id', 'category_id');
    }
}
