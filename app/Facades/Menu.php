<?php

namespace App\Facades;

use Storage;

class Menu
{
    public function getByName($name)
    {
        if (isset(json_decode(Storage::disk('local')->get('menu'))->{$name})) {
            return json_decode(Storage::disk('local')->get('menu'))->{$name};
        }
        return false;
    }
    
    public function getByType($name)
    {
        if (isset(json_decode(Storage::disk('local')->get('menu'))->{$name})) {
            return json_decode(Storage::disk('local')->get('menu'))->{$name};
        }
        return false;
    }
}
